extends Node2D

onready var player_scene = load("res://Player/Player.tscn")
var guns = []
var shields = []
var ammo = []
var pickups = []

func _ready():
	$Border.rect_size = globals.BOUNDS.size + 2 * Vector2(256,256) # Thickness of walls added, * 2 to account for offset below
	$Border.rect_position = Vector2(-256,-256) # Subtract thickness of walls
	
	# Load all scenes into mem
	for gun_id in globals.data["weapons"]:
		guns.append(load(globals.data["weapons"][String(gun_id)]["scene"]))
	for shield_id in globals.data["shields"]:
		shields.append(load(globals.data["shields"][String(shield_id)]["scene"]))
	for ammo_id in globals.data["ammo"]:
		ammo.append(load(globals.data["ammo"][String(ammo_id)]["scene"]))
	for pickup_id in globals.data["pickups"]:
		pickups.append(load(globals.data["pickups"][String(pickup_id)]["scene"]))


func _input(event):
	if Input.is_action_pressed("escape"):
		gamestate.exit_game()


# Call with where to spawn and id of player spawning
puppet func spawn_player(spawn_pos, spawn_id, gun_id, shield_id, ability_id, is_bot=false):
	# Spawn this one on all peers, setting network master as appropriate
	# Setup player
	var player = player_scene.instance()
	player.SPEED_MOVE = globals.data["player"]["speed_move"]
	player.TELLEPORT_RANGE = globals.data["abilities"]["1"]["range"]
	player.MAX_HEALTH = globals.data["player"]["health"]
	player.health = player.MAX_HEALTH
	player.id = spawn_id
	player.position = spawn_pos
	
	# Imp: Set unique node name
	if is_bot:
		player.name = "bot_" + str(spawn_id)
	else: 
		player.name = "player_" + str(spawn_id)
	
	
	if spawn_id == get_tree().get_network_unique_id():
		# If node for this peer id, set name
		player.set_player_name("You")
	else:
		player.set_player_name(gamestate.players[spawn_id]["name"])
	
	
	# Setup gun
	var gun = guns[int(gun_id)].instance()
	var gun_dict = globals.data["weapons"][String(gun_id)]
	var offset = Vector2( gun_dict["offset_x"], gun_dict["offset_y"])
	
	gun.id_player = spawn_id
	gun.MAX_BULLETS = gun_dict["bullets"]
	gun.bullet_count = gun.MAX_BULLETS
	player.set_gun(gun, offset)
	
	
	# Setup shield
	var shield = shields[int(shield_id)].instance()
	shield.RECHARGE_RATE = globals.data["shields"][String(shield_id)]["regen_rate"]
	shield.MAX_HEALTH = globals.data["shields"][String(shield_id)]["health"]
	shield.health = shield.MAX_HEALTH
	player.set_shield(shield)
	
	# Setup contoller
	if get_tree().get_network_unique_id() == spawn_id:
		$PlayerController.ability_id = String(ability_id)
		$PlayerController/AbilityCooldown.wait_time = globals.data["abilities"][ability_id]["cooldown"]
		$PlayerController/FireDelay.wait_time = gun_dict["fire_rate"]
		$PlayerController.set_player(player)
	
	# Finally add to scenetree :D
	if globals.node_Players.has_node(String(spawn_id)):
		remove_player(spawn_id)
	globals.node_Players.add_child(player)


puppet func spawn_crate(spawn_pos, crate_id, crate_idx, hidden=false):
	var crate = pickups[crate_id].instance()
	
	crate.position = spawn_pos
	crate.name = String(crate_idx)
	if hidden:
		crate.hide() # Hides
	
	globals.node_world.get_node("Pickups").add_child(crate)


puppet func spawn_bullet(pos, rot, id, index, ammo_id):
	var new_ammo = ammo[ammo_id].instance()
	new_ammo.name = String(index)
	new_ammo.position = pos
	new_ammo.rotation = rot
	new_ammo.set_id_origin(id)
	
	if ammo_id == 0 or ammo_id == 1:
		new_ammo.BULLET_SPEED = globals.data["ammo"][String(ammo_id)]["bullet_speed"]
	elif ammo_id == 3:
		new_ammo.laser_size = globals.data["ammo"]["3"]["laser_size"]
	
	globals.node_bullet_container.add_child(new_ammo)


puppet func remove_player(id):
	var player = $Players.get_node("player_" + String(id))
	$Players.remove_child(player)
	
	player.queue_free()

