extends Area2D

class_name Pickup # TODO: add , "path/to/class/image"

puppet func pickup():
	hide() # TODO: Make sure can no longer pick up
	$CollisionShape2D.set_deferred("disabled", true) # Do we even need collisionshape client side?