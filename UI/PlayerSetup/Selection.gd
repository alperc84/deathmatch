extends Control

var selection = null


func set_title(title:String):
	$VBox/Title.text = title


func add_item(item:Control):
	item.connect("selected", self, "update_selection")
	$VBox/PanelCont/VBox.add_child(item)


func update_selection(new_selection:Control):
	if selection != null:
		selection.self_modulate = Color.white
	new_selection.self_modulate = Color.lightgreen
	
	selection = new_selection


func get_selected_type():
	return selection.identifier


func is_anything_selected():
	return selection != null