extends PanelContainer

var PropertyInfo = preload("res://UI/PlayerSetup/PropertyInfo.tscn")

var identifier
var waiting_mouse_click = false

signal selected(identifier)


func set_title(title:String):
	$VBox/Title.text = title


func set_description(description:String):
	$VBox/HBox/Panel/VBox/Description.text = description


func set_icon(texture:Texture):
	$VBox/HBox/TextureRect.texture = texture


func add_property(name:String, info:String):
	var property = PropertyInfo.instance()
	
	property.name = name
	property.set_title(name)
	property.set_info(info)
	
	$VBox/HBox/Panel/VBox/Properties.add_child(property)


func mouse_inside_control(is_inside):
	waiting_mouse_click = is_inside

func _input(event):
	if waiting_mouse_click and event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		emit_signal("selected", self)
