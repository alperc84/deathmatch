extends MarginContainer

var scores_flatter = []


func _ready(): 
	hide() # Should already be hidden, but just in case
	gamestate.connect("latency_update", self, "update_ping_label")


func _process(delta):
	if Input.is_action_just_pressed("view_leaderboard"):
		show_leaderboard()
	elif Input.is_action_just_released("view_leaderboard"):
		hide()


func show_leaderboard():
	if gamestate.players.has(get_tree().get_network_unique_id()):
		gamestate.players[get_tree().get_network_unique_id()]["name"] = "Me" # Yea it's hack, deal with it
	
	sort_scores()
	var display_text = ""
	for pair in scores_flatter:
		display_text += pair["name"] + " has " + str(pair["total_kills"]) + " frags and is on a " + str(pair["kill_streak"]) +" killstreak!\n"
	$VCont/Display.text = display_text
	show()


func sort_scores():
	# O(n^2) fine cause small n
	scores_flatter.clear()
	for id in gamestate.players:
		scores_flatter.append(gamestate.players[id])
	scores_flatter.sort_custom(CustomSort, "score_leq")


func update_ping_label():
	var label_ping = $VCont/Ping
	label_ping.text = "Ping: " + String(gamestate.latency)
	
	# Arbitrary latency vals
	if gamestate.latency < 70:
		label_ping.modulate = Color.lightgreen
	elif gamestate.latency < 120:
		label_ping.modulate = Color.lightyellow
	else:
		label_ping.modulate = Color.red


class CustomSort:
	static func score_leq(id_a,id_b):
		return id_a["kill_streak"] >= id_b["kill_streak"]