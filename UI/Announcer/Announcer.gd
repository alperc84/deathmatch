extends Label

func _process(delta):
	if visible:
#		print(modulate)
		pass

func announce(_text):
	modulate = Color.white
	text = _text
	$Tween.interpolate_property(self, "modulate", null, Color(1,1,1,0), 5, Tween.TRANS_EXPO, Tween.EASE_IN)
	$Tween.start()
	show()

func _on_Tween_tween_all_completed():
	hide()
