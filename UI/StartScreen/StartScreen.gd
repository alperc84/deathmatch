extends CanvasLayer

func hide():
	$MainMenu.hide()
	$Credits.hide()
	$Options.hide()

func show():
	$MainMenu.show()


func _on_MainMenu_credits_pressed():
	$Credits.popup_centered()


func _on_MainMenu_options_pressed():
	$Options.show()
