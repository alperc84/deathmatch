extends MarginContainer

var edit_action
var new_action_event = null

func _ready():
	$CenterContainer/VBox/ability/Hotkey.text = (InputMap.get_action_list("ability")[0]).as_text()
	$CenterContainer/VBox/view_leaderboard/Hotkey.text = (InputMap.get_action_list("view_leaderboard")[0]).as_text()
	
	$CenterContainer/VBox/ability/Hotkey.connect("pressed", self, "_on_edit_action", ["ability"])
	$CenterContainer/VBox/view_leaderboard/Hotkey.connect("pressed", self, "_on_edit_action", ["view_leaderboard"])


func _input(event):
	if event is InputEventKey and event.pressed:
		$HotkeyEdit/VBox/Keys.text = event.as_text()
		new_action_event = event


func _on_OKButton_pressed():
	globals.save_settings()
	hide()


func _on_action_update(action):
	var action_node = get_node("CenterContainer/VBox/" + action)
	action_node.get_node("Hotkey").text = (InputMap.get_action_list(action)[0]).as_text()


func _on_edit_action(action):
	$HotkeyEdit.popup_centered()
	edit_action = action


func _on_Cancle_pressed():
	$HotkeyEdit.hide()


func _on_Confirm_pressed():
	if new_action_event != null:
		InputMap.action_erase_events(edit_action) # Warning: nukes all current actions
		InputMap.action_add_event(edit_action, new_action_event)
		_on_action_update(edit_action)
		
		$HotkeyEdit.hide()